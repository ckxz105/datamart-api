.. _python:

Python API
**********

Searching a Datamart
====================

.. autoclass:: datamart.Datamart
   :members:

Search results
==============

.. autoclass:: datamart.DatamartQueryCursor
   :members:

.. autoclass:: datamart.DatamartSearchResult
   :members:

Search query
============

.. autoclass:: datamart.DatamartQuery
   :members:

.. autoclass:: datamart.TabularVariable
   :members:

.. autoclass:: datamart.ColumnRelationship
   :members:
   :undoc-members:

.. autoclass:: datamart.VariableConstraint
   :members:

.. autoclass:: datamart.NamedEntityVariable
   :members:

.. autoclass:: datamart.TemporalVariable
   :members:

.. autoclass:: datamart.TemporalGranularity
   :members:
   :undoc-members:

.. autoclass:: datamart.GeospatialVariable
   :members:

.. autoclass:: datamart.GeospatialGranularity
   :members:
   :undoc-members:

Augmentation specification
==========================

.. autoclass:: datamart.AugmentSpec
   :members:

.. autoclass:: datamart.TabularJoinSpec
   :members:

.. autoclass:: datamart.UnionSpec
   :members:

.. autoclass:: datamart.DatasetColumn
   :members:
