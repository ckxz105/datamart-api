.. _query_input_sample:

Query Input Samples
*******************

.. literalinclude:: query_input_sample.json
   :language: json
   :linenos:

.. literalinclude:: query_input_sample_taxi.json
   :language: json
   :linenos:

.. literalinclude:: query_input_sample_fifa.json
   :language: json
   :linenos:
