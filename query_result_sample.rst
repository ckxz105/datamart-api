.. _query_result_sample:

Query Result Sample
*******************

.. literalinclude:: query_result_sample.json
   :language: json
   :linenos:
